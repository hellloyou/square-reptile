import requests
from login import Login

def main():
    url = 'http://jwgl.gdust.edu.cn/'  # 登录网址，网址最后务必加上'/'，否则无法识别

    # xh = str(input("请输入学号："))  # 登录账号
    # pw = str(input("请输入密码："))  # 登录密码
    

    # 登录教务系统
    url_head = str(url)[0:50]  # 网站会自动跳转，获取网址前缀
    login = Login(xh, pw, url)
    xm = login.login_website
    print(xm)
    if xm != '登录失败':
        #抢课
        login.post_xxk(xm)
        #课表
        login.get_table_time(xm)
        #选修课列表
        login.get_xxk_list(xm)
        #在校成绩
        login.getSchoolGrades(xm)

if __name__ == "__main__":
    main()
