from flask import Flask, url_for, request, redirect, json

from login import Login
from predictor import split_pic, analyse
from tensorflow import keras

app = Flask(__name__)  # __main__
model = keras.models.load_model('./model/Model_tf.net')


@app.route("/")
def hello(name=None):
    return """home page
    upload: /upload
    api: /api
    """


@app.route('/upload', methods=['GET', 'POST'])
def upload_file():
    if request.method == 'POST':
        # check if the post request has the file part
        if 'file' not in request.files:
            return 'No file part'
        file = request.files['file']
        # if user does not select file, browser also
        # submit an empty part without filename
        if file.filename == '':
            return 'No selected file'
        else:
            result = analyse(file.read(), model)
            return "".join(result)
    elif request.method == 'GET':
        return '''
        <!doctype html>
        <title>Upload new File</title>
        <h1>Upload new File</h1>
        <form method=post enctype=multipart/form-data>
        <input type=file name=file>
        <input type=submit value=Upload>
        </form>
        '''


@app.route('/api', methods=['POST'])
def api():
    if request.method == 'POST':
        stream = request.data
        result = analyse(stream, model)
        return "".join(result)


@app.route('/getGradeReport', methods=['GET'])
def getGradeReport():
    url = 'http://jwgl.gdust.edu.cn/'  # 登录网址，网址最后务必加上'/'，否则无法识别
    xh = request.args.get("xh")
    pw = request.args.get("pw")

    # 登录教务系统
    url_head = str(url)[0:50]  # 网站会自动跳转，获取网址前缀
    login = Login(xh, pw, url)
    xm = login.login_website
    print(xm)
    if xm != '登录失败':
        return json.dumps(login.getSchoolGrades(xm))
    return "登录失败"

@app.route('/getCourse', methods=['GET'])
def getCourse():
    url = 'http://jwgl.gdust.edu.cn/'  # 登录网址，网址最后务必加上'/'，否则无法识别
    xh = request.args.get("xh")
    pw = request.args.get("pw")

    # 登录教务系统
    url_head = str(url)[0:50]  # 网站会自动跳转，获取网址前缀
    login = Login(xh, pw, url)
    xm = login.login_website
    print(xm)
    if xm != '登录失败':
        # login.get_table_time(xm)
        # login.get_xxk_list(xm)
        # login.getSchoolGrades(xm)
        # return xm
        # result = {'courseList': login.getSchoolGrades(xm)}
        # print(json.dumps(result))
        return json.dumps(login.get_table_time(xm))
    return "登录失败"

@app.route('/login', methods=['GET'])
def login():
    url = 'http://jwgl.gdust.edu.cn/'  # 登录网址，网址最后务必加上'/'，否则无法识别

    xh = request.args.get("xh")
    pw = request.args.get("pw")

    # 登录教务系统
    url_head = str(url)[0:50]  # 网站会自动跳转，获取网址前缀
    login = Login(xh, pw, url)
    xm = login.login_website
    print(xm)
    if xm != '登录失败':
        # login.get_table_time(xm)
        # login.get_xxk_list(xm)
        return xm
        # return json.dumps(login.getSchoolGrades(xm))
    return "登录失败"

if __name__ == "__main__":
    from gevent.pywsgi import WSGIServer

    http_server = WSGIServer(('localhost', 5000), app)
    http_server.serve_forever()
